{
    'name': "Odoo Facto customizations",
    'version': '12.0.0.0.3',
    'depends': ['account_payment_order'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Cooperative management",
    'description': """
    Odoo Facto customizations.
    """,
    "license": "AGPL-3",
    'data': [
    ],
}
